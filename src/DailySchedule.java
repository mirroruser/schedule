import java.util.ArrayList;
import java.util.List;

public class DailySchedule {
    private List<Appointment> apptList;

    public DailySchedule() {
        this.apptList = new ArrayList<>();
    }

    public void clearConflicts(Appointment appt) {
        List<Appointment> newList = new ArrayList<>();

        for (Appointment curAppt : this.apptList) {
            if (!appt.conflictsWith(curAppt)) newList.add(curAppt);
        }
        this.apptList = newList;
    }

    public boolean addAppt(Appointment appt, boolean emergency) {
        if (emergency) {
            this.clearConflicts(appt);
            this.apptList.add(appt);
            return true;
        } else {
            boolean conflicts = false;
            for (Appointment curAppt : this.apptList) {
                if (appt.conflictsWith(curAppt) && !conflicts) conflicts = true;
            }
            if (!conflicts) this.apptList.add(appt);
            return conflicts;
        }
    }

    public String toString() {
        return this.apptList.toString();
    }
}
