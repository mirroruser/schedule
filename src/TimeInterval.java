public class TimeInterval {
    private double t0;
    private double t1;

    public TimeInterval(double t0, double t1) {
        this.t0 = t0;
        this.t1 = t1;
    }

    public boolean overlapsWith(TimeInterval interval) {
        return interval.getT0() > this.t0 && interval.getT0() < this.t1 ||
                interval.getT1() > this.t0 && interval.getT1() < this.t1;
    }

    public double getT0() {
        return this.t0;
    }

    public double getT1() {
        return this.t1;
    }
}
