public class Appointment {
    private TimeInterval time;

    public Appointment(double t0, double t1) {
        this.time = new TimeInterval(t0, t1);
    }

    public boolean conflictsWith(Appointment other) {
        return this.time.overlapsWith(other.getTime());
    }

    public TimeInterval getTime() {
        return this.time;
    }

    public String toString() {
        return "Start: " + this.time.getT0() + ", End: " + this.time.getT1() + "\n";
    }
}
